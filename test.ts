import { Files } from './files';

async function test1(run: boolean) {
    if (run) {
        const files = await new Files().fromDirectory("/Volumes/RED2/Applications/", ['-type', 'f', '-size', '+1G'])
        const movies = files.byTypes(['movie']);
        // for (const file of movies) {
        //     console.log(file.filename);
        // }
        console.log('test1', movies.length);
    }
}

async function test2(run: boolean) {
    if (run) {
        const files = await new Files().fromDirectory("/Volumes/RED3/Applications/", ['-type', 'f', '-size', '+1G'])
        const movies = files.byTypes(['movie']);
        // for (const file of movies) {
        //     console.log(file.filename);
        // }
        console.log('test2', movies.length);
    }
}

async function test3(run: boolean) {
    if (run) {
        const dirs = [
            "/Volumes/disc1/Temp",
            "/Volumes/disc2/Applications",
            "/Volumes/disc2/transmission/complete",
            "/Volumes/disc3/Applications",
            "/Volumes/RED1/Downloads/_complete",
            "/Volumes/RED2/Applications/",
            "/Volumes/RED3/Applications/",
            "/Volumes/RED4/Applications"
        ]
        const files = await new Files().fromDirectories(dirs, ['-type', 'f', '-size', '+1G'])
        const movies = files.byTypes(['movie']);
        // for (const file of movies) {
        //     console.log(file.path);
        // }
        console.log('test3', movies.length);
    }
}

async function test4(run: boolean) {
    if (run) {
        const dirs = [
            "/Volumes/disc3/Applications/"
        ]
        const files = await new Files().fromDirectories(dirs, ['-type', 'f', '-iname', '*-thumbs.jpg'])
        const images = files.byTypes(['image']);
        for (const file of images) {
            // console.log(file.path);
            const dimensions = await file.imageinfo();
            if (dimensions.width && dimensions.width !== 3408) {
				console.log(file.path);
			}

        }
        console.log('test4', images.length);
    }
}

test1(false);
test2(false);
test3(false);
test4(false);