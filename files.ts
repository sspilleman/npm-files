import * as fs from 'fs';
import * as path from 'path';
import * as crypto from 'crypto';
import * as mm from 'music-metadata';
import * as imageSize from 'image-size';

import { getFfprobe, getFilesFromDir, dirRemoveLastSlash } from './lib/utils';

export class File {
	public path: string;
	public size: number;
	public dir: string;
	public filename: string;
	public ext: string;
	public base: string;
	public type: string = '';
	public sha1: string = '';
	public audio: any;
	public image: any;
	public video: any;

	constructor(filepath: string, private debug: boolean = true) {
		this.path = filepath;
		// stats
		const stats = fs.statSync(filepath);
		this.size = stats.size;
		// path elements
		const parsed = path.parse(filepath);
		this.dir = parsed.dir;
		this.filename = parsed.base;
		this.ext = parsed.ext;
		this.base = parsed.name;
		if (this.isValid()) {
			this.getType();
		}
	}

	getType() {
		const lower = this.ext.toLowerCase();
		if (['.nfo', '.txt', '.diz'].indexOf(lower) !== -1) this.type = 'nfo';
		if (['.mp4', '.wmv', '.mkv', '.mov', '.m4v', '.avi', '.divx', '.flv', '.f4v', '.asf', '.mpg', '.mpeg', '.ts', '.3gp', '.webm', '.rm', '.m2ts'].indexOf(lower) !== -1) this.type = 'movie';
		if (['.srt', '.sub', '.idx'].indexOf(lower) !== -1) this.type = 'subtitle';
		if (['.mp3', '.flac', '.wav', '.m4a', '.m3u', '.cue'].indexOf(lower) !== -1) this.type = 'music';
		if (['.jpg', '.jpeg', '.png', '.gif', '.tif', '.tiff', '.bmp', '.ico', '.svg', '.eps', '.psd', '.dcr', '.ai', '.icns', '.ico'].indexOf(lower) !== -1) this.type = 'image';
		if (['.rar', '.zip', '.par', '.par2', '.gz', '.7z', '.tgz'].indexOf(lower) !== -1) this.type = 'archive';
		if (/\.r[0-9]{1,}/i.test(lower)) {
			this.type = 'archive';
		}
		if (/\.[0-9]{2,}/i.test(lower)) {
			this.type = 'filepart';
		}
		if (['.dmg', '.iso', '.bin', '.img'].indexOf(lower) !== -1) this.type = 'media';
		if (['.exe', '.sh'].indexOf(lower) !== -1) this.type = 'executable';
		if (['.webloc', '.html', '.htm', '.xml', '.css', '.scss', '.coffee', '.js', '.json', '.less', '.py', '.md', '.jmx', '.cs', '.cc', '.h', '.cs', '.yml', '.c'].indexOf(lower) !== -1) this.type = 'html';
		if (['.pdf', '.epub', '.cbr', '.cbz', '.mobi', '.rtf'].indexOf(lower) !== -1) this.type = 'ebook';
		if (['.torrent', '.nzb'].indexOf(lower) !== -1) this.type = 'download';
		if (['.ppt', '.pptx', '.doc', '.docx', '.xls', '.xlsx'].indexOf(lower) !== -1) this.type = 'office';
		if (['.woff', '.ttf', '.otf', '.woff2', '.eot'].indexOf(lower) !== -1) this.type = 'font';
		if (['.m3u', '.cue', '.log', '.bup', '.ifo', '.vob', '.db', '.plist', '.ics', '.safariextz', '.vcf', '.pm6', '.scpt', '.dll', '.db', '.db', '.db'].indexOf(lower) !== -1) this.type = 'ignore';
		if (['.sfv', '.srr', '.url', '.md5'].indexOf(lower) !== -1) this.type = 'garbage';
		if (lower === '') this.type = 'unknown';
		if (!this.type) {
			if (this.debug) console.log('unknown type', lower, this.path);
		}
		if (this.type === 'unknown') {
			if (this.debug) console.log('unknown type', this.path);
		}
	}

	isValid() {
		const lower = this.filename.toLowerCase();
		let valid = true;
		if (lower.startsWith('.ds_store')) {
			valid = false;
		}
		if (lower.startsWith('._')) {
			valid = false;
		}
		return valid;
	}

	async sha(divider: number) {
		return new Promise(resolve => {
			var processfilesize = Math.round(this.size / divider);
			var shasum = crypto.createHash('sha1');
			var readable = fs.createReadStream(this.path, {
				start: 0,
				end: processfilesize
			});
			readable.on('data', function (d) {
				shasum.update(d);
			});
			readable.on('end', () => {
				var d = shasum.digest('hex');
				this.sha1 = d;
				return resolve(d);
			});
		});
	}

	async imageinfo() {
		const types = ['bmp', 'cur', 'dds', 'gif', 'icns', 'ico', 'j2c', 'jp2', 'jpg', 'ktx', 'png', 'pnm', 'psd', 'svg', 'tiff', 'webp'];
		if (types.indexOf(this.ext.slice(1)) !== -1) {
			this.image = imageSize.imageSize(this.path); // imageSize.types; // (this.path);
			return this.image;
		} else {
			console.log(`unknown extension ${this.path}`);
			return undefined;
		}
	}

	async movieinfo() {
		const ffprobe = await getFfprobe(this.path);
		var height = 0;
		var width = 0;
		ffprobe.streams.forEach((stream: any) => {
			if (stream.width && stream.width > width) width = stream.width;
			if (stream.height && stream.height > height) height = stream.height;
		});
		this.video = Object.assign({}, ffprobe, {
			width: width,
			height: height,
			duration: parseFloat(ffprobe.format.duration)
		})
		// this.video = {
		// 	width: width,
		// 	height: height,
		// 	duration: parseFloat(ffprobe.format.duration)
		// }
		return this.video;
	}

	async musicinfo() {
		return mm.parseFile(this.path, {
			skipCovers: true,
			duration: true,
			skipPostHeaders: true,
			includeChapters: false
		})
			.then((metadata: mm.IAudioMetadata) => {
				delete metadata.common.picture;
				this.audio = Object.assign({}, metadata.format, metadata.common);
				// console.log(util.inspect(msg, { showHidden: false, depth: null }));
				return this.audio;
			})
			.catch(function (err: any) {
				console.error(err.message);
			});
	}
}

export class Files {

	private _files: Array<File> = [];
	private _sources: Array<string> = [];

	constructor(private debug: boolean = true) { }

	async fromDirectory(dir: string, options: Array<string>) {
		dir = dirRemoveLastSlash(dir);
		this._sources.push(dir);
		// console.log('Reading from Directory', dir, 'with options', options);
		const files = await getFilesFromDir(dir, options);
		files.forEach(filepath => {
			const file = new File(filepath, this.debug);
			if (file.isValid()) {
				this._files.push(file);
			} else {
				// console.log('invalid', file.path);
			}
		});
		if (this.debug) console.log('done', dir, options, files.length);
		return this;
	}

	async fromDirectories(dirs: Array<string>, options: Array<string>): Promise<Files> {
		return Promise.all(dirs.map(dir => this.fromDirectory(dir, options))).then(() => this);
	}

	get all() {
		return this._files;
	}

	get length() {
		return this._files.length;
	}

	get filetypes() {
		return this._files.reduce((prev: Array<string>, current: File, index) => {
			if (prev.indexOf(current.type) === -1) {
				prev.push(current.type);
			}
			return prev;
		}, []);
	}

	byType(type: string): Array<File> {
		return this._files.filter(file => file.type === type)
	}

	byTypes(types: Array<string>): Array<File> {
		return this._files.filter(file => types.indexOf(file.type) !== -1)
	}

	// byRegex(field: string, regex: RegExp): Array<File> {
	// 	return this._files.filter(file => regex.test(file[field].toString()))
	// }

	withoutType(type: string): Array<File> {
		return this._files.filter(file => file.type !== type)
	}

	withoutTypes(types: Array<string>): Array<File> {
		return this._files.filter(file => types.indexOf(file.type) === -1);
	}

	findRelated(inputfile: File): Array<File> {
		if (['movie', 'nfo'].indexOf(inputfile.type) === -1) {
			throw new Error('dont know how to find related of ' + inputfile.type);
		}
		let related: Array<File> = [];
		if (inputfile.type === 'movie') {
			related = this._files.filter(file => {
				let letthrough = false;
				if (file.path !== inputfile.path && file.dir === inputfile.dir && file.type !== 'movie') {
					if (file.base === inputfile.base || file.base === inputfile.base + '-thumbs') {
						letthrough = true;
					}
				}
				return letthrough;
			});
		}
		if (inputfile.type === 'nfo') {
			related = this._files.filter(file => {
				let letthrough = false;
				if (file.path !== inputfile.path && file.dir === inputfile.dir && file.type !== 'nfo') {
					if (file.base === inputfile.base || file.base === inputfile.base + '-thumbs') {
						letthrough = true;
					}
				}
				return letthrough;
			});
		}
		if (this.debug) {
			if (related.length > 2) {
				related.forEach(file => {
					console.log(inputfile.path, file.type, file.path);
				});
				console.log();
			}
		}
		return related;
	}
}
