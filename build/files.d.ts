export declare class File {
    private debug;
    path: string;
    size: number;
    dir: string;
    filename: string;
    ext: string;
    base: string;
    type: string;
    sha1: string;
    audio: any;
    image: any;
    video: any;
    constructor(filepath: string, debug?: boolean);
    getType(): void;
    isValid(): boolean;
    sha(divider: number): Promise<unknown>;
    imageinfo(): Promise<any>;
    movieinfo(): Promise<any>;
    musicinfo(): Promise<any>;
}
export declare class Files {
    private debug;
    private _files;
    private _sources;
    constructor(debug?: boolean);
    fromDirectory(dir: string, options: Array<string>): Promise<this>;
    fromDirectories(dirs: Array<string>, options: Array<string>): Promise<Files>;
    get all(): File[];
    get length(): number;
    get filetypes(): string[];
    byType(type: string): Array<File>;
    byTypes(types: Array<string>): Array<File>;
    withoutType(type: string): Array<File>;
    withoutTypes(types: Array<string>): Array<File>;
    findRelated(inputfile: File): Array<File>;
}
