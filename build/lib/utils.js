"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
// import { stream } from 'stream';
var JSONStream = __importStar(require("JSONStream"));
var bl_1 = __importDefault(require("bl"));
var filesize_1 = __importDefault(require("filesize"));
function dirRemoveLastSlash(dir) {
    return dir.endsWith('/') ? dir.slice(0, -1) : dir;
}
exports.dirRemoveLastSlash = dirRemoveLastSlash;
function getFfprobe(filePath) {
    // ffprobe -v error -show_format -show_streams -print_format json file.mpg 
    return new Promise(function (resolve, reject) {
        var info;
        var stderr;
        var params = [];
        // params.push('-show_format', '-show_streams', '-show_entries', 'stream_tags:format_tags', '-print_format', 'json', filePath);
        params.push('-v', 'quiet', '-print_format', 'json', '-show_format', '-show_streams', '-hide_banner', '-i', filePath);
        var ffprobe = child_process_1.spawn('ffprobe', params);
        ffprobe.once('close', function (code) {
            if (!code) {
                // console.log("code", code);
                return resolve(info);
            }
            else {
                var error = stderr.split('\n').filter(Boolean).pop();
                console.log("Error:", filePath, error);
                return resolve(null);
            }
        });
        if (ffprobe && ffprobe.stderr && ffprobe.stdout) {
            ffprobe.stderr
                .pipe(new bl_1.default(function (err, data) {
                stderr = data.toString();
            }));
            ffprobe.stdout
                .pipe(JSONStream.parse(undefined))
                .once('data', function (data) {
                info = data;
            });
        }
    });
}
exports.getFfprobe = getFfprobe;
// getInfo('/Volumes/Data/Downloads/Mysteryland Maarten/20130824_224844.mp4').then(info => {
//     console.log(info);
// })
function getFilesFromDir(dir, options) {
    return new Promise(function (resolve, reject) {
        child_process_1.execFile('find', [dir].concat(options), {
            maxBuffer: 16384 * 2048 * 2048
        }, function (err, stdout, stderr) {
            if (err || stderr) {
                console.log("err", err);
                console.log("stderr", stderr);
                return resolve([]);
            }
            var file_list = stdout.split('\n');
            file_list.pop();
            return resolve(file_list);
        });
    });
}
exports.getFilesFromDir = getFilesFromDir;
;
function toHumanFilesize(size) {
    return filesize_1.default(size, {
        base: 10
    });
}
exports.toHumanFilesize = toHumanFilesize;
// function dynamicSort(property: string) {
//     var sortOrder = 1;
//     if (property[0] === "-") {
//         sortOrder = -1;
//         property = property.substr(1);
//     }
//     return function (a, b) {
//         // console.log(a[property]);
//         var _a = eval('a.' + property);
//         var _b = eval('b.' + property);
//         var result = (_a < _b) ? -1 : (_a > _b) ? 1 : 0;
//         return result * sortOrder;
//     }
// }
// export function dynamicSortMultiple() {
//     /*
//      * save the arguments object as it will be overwritten
//      * note that arguments object is an array-like object
//      * consisting of the names of the properties to sort by
//      */
//     var props = arguments;
//     return function (obj1, obj2) {
//         var i = 0,
//             result = 0,
//             numberOfProperties = props.length;
//         /* try getting a different result from 0 (equal)
//          * as long as we have extra properties to compare
//          */
//         while (result === 0 && i < numberOfProperties) {
//             result = dynamicSort(props[i])(obj1, obj2);
//             i++;
//         }
//         return result;
//     }
// }
// export function setVariableInterval(callbackFunc, timing) {
//     var variableInterval = {
//         interval: timing,
//         callback: callbackFunc,
//         timeout: undefined,
//         stopped: false,
//         runLoop: function () {
//             if (variableInterval.stopped) return;
//             var result = variableInterval.callback.call(variableInterval);
//             if (typeof result == 'number') {
//                 if (result === 0) return;
//                 variableInterval.interval = result;
//             }
//             variableInterval.loop();
//         },
//         stop: function () {
//             this.stopped = true;
//             clearTimeout(this.timeout);
//         },
//         start: function () {
//             this.stopped = false;
//             return this.loop();
//         },
//         loop: function () {
//             this.timeout = setTimeout(this.runLoop, this.interval);
//             return this;
//         }
//     };
//     return variableInterval.start();
// };
// function FixedQueue(size, initialValues) {
//     initialValues = (initialValues || []);
//     var queue = Array.apply(null, initialValues);
//     queue.fixedSize = size;
//     queue.push = FixedQueue.push;
//     queue.splice = FixedQueue.splice;
//     queue.unshift = FixedQueue.unshift;
//     FixedQueue.trimTail.call(queue);
//     return (queue);
// }
// FixedQueue.trimHead = function () {
//     if (this.length <= this.fixedSize) {
//         return;
//     }
//     Array.prototype.splice.call(
//         this,
//         0, (this.length - this.fixedSize)
//     );
// };
// FixedQueue.trimTail = function () {
//     if (this.length <= this.fixedSize) {
//         return;
//     }
//     Array.prototype.splice.call(
//         this,
//         this.fixedSize, (this.length - this.fixedSize)
//     );
// };
// FixedQueue.wrapMethod = function (methodName, trimMethod) {
//     // Create a wrapper that calls the given method.
//     var wrapper = function () {
//         // Get the native Array method.
//         var method = Array.prototype[methodName];
//         // Call the native method first.
//         var result = method.apply(this, arguments);
//         // Trim the queue now that it's been augmented.
//         trimMethod.call(this);
//         // Return the original value.
//         return (result);
//     };
//     return (wrapper);
// };
// FixedQueue.push = FixedQueue.wrapMethod(
//     "push",
//     FixedQueue.trimHead
// );
// FixedQueue.splice = FixedQueue.wrapMethod(
//     "splice",
//     FixedQueue.trimTail
// );
// FixedQueue.unshift = FixedQueue.wrapMethod(
//     "unshift",
//     FixedQueue.trimTail
// );
// function setDeceleratingTimeout(callback, factor, times) {
//     var internalCallback = function(t, counter) {
//         return function() {
//             if (--t > 0) {
//                 setTimeout(internalCallback, ++counter * factor);
//                 callback();
//             }
//         }
//     }(times, 0);
//     setTimeout(internalCallback, factor);
// };
// setDeceleratingTimeout(function() {
//     console.log('hi');
// }, 1, 1000);
// setDeceleratingTimeout(function() {
//     console.log('bye');
// }, 10, 1000);
//# sourceMappingURL=utils.js.map