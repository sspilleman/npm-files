export declare function dirRemoveLastSlash(dir: string): string;
export declare function getFfprobe(filePath: string): Promise<any>;
export declare function getFilesFromDir(dir: string, options: Array<any>): Promise<Array<string>>;
export declare function toHumanFilesize(size: number): string;
