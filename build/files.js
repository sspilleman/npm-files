"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var crypto = __importStar(require("crypto"));
var mm = __importStar(require("music-metadata"));
var imageSize = __importStar(require("image-size"));
var utils_1 = require("./lib/utils");
var File = /** @class */ (function () {
    function File(filepath, debug) {
        if (debug === void 0) { debug = true; }
        this.debug = debug;
        this.type = '';
        this.sha1 = '';
        this.path = filepath;
        // stats
        var stats = fs.statSync(filepath);
        this.size = stats.size;
        // path elements
        var parsed = path.parse(filepath);
        this.dir = parsed.dir;
        this.filename = parsed.base;
        this.ext = parsed.ext;
        this.base = parsed.name;
        if (this.isValid()) {
            this.getType();
        }
    }
    File.prototype.getType = function () {
        var lower = this.ext.toLowerCase();
        if (['.nfo', '.txt', '.diz'].indexOf(lower) !== -1)
            this.type = 'nfo';
        if (['.mp4', '.wmv', '.mkv', '.mov', '.m4v', '.avi', '.divx', '.flv', '.f4v', '.asf', '.mpg', '.mpeg', '.ts', '.3gp', '.webm', '.rm', '.m2ts'].indexOf(lower) !== -1)
            this.type = 'movie';
        if (['.srt', '.sub', '.idx'].indexOf(lower) !== -1)
            this.type = 'subtitle';
        if (['.mp3', '.flac', '.wav', '.m4a', '.m3u', '.cue'].indexOf(lower) !== -1)
            this.type = 'music';
        if (['.jpg', '.jpeg', '.png', '.gif', '.tif', '.tiff', '.bmp', '.ico', '.svg', '.eps', '.psd', '.dcr', '.ai', '.icns', '.ico'].indexOf(lower) !== -1)
            this.type = 'image';
        if (['.rar', '.zip', '.par', '.par2', '.gz', '.7z', '.tgz'].indexOf(lower) !== -1)
            this.type = 'archive';
        if (/\.r[0-9]{1,}/i.test(lower)) {
            this.type = 'archive';
        }
        if (/\.[0-9]{2,}/i.test(lower)) {
            this.type = 'filepart';
        }
        if (['.dmg', '.iso', '.bin', '.img'].indexOf(lower) !== -1)
            this.type = 'media';
        if (['.exe', '.sh'].indexOf(lower) !== -1)
            this.type = 'executable';
        if (['.webloc', '.html', '.htm', '.xml', '.css', '.scss', '.coffee', '.js', '.json', '.less', '.py', '.md', '.jmx', '.cs', '.cc', '.h', '.cs', '.yml', '.c'].indexOf(lower) !== -1)
            this.type = 'html';
        if (['.pdf', '.epub', '.cbr', '.cbz', '.mobi', '.rtf'].indexOf(lower) !== -1)
            this.type = 'ebook';
        if (['.torrent', '.nzb'].indexOf(lower) !== -1)
            this.type = 'download';
        if (['.ppt', '.pptx', '.doc', '.docx', '.xls', '.xlsx'].indexOf(lower) !== -1)
            this.type = 'office';
        if (['.woff', '.ttf', '.otf', '.woff2', '.eot'].indexOf(lower) !== -1)
            this.type = 'font';
        if (['.m3u', '.cue', '.log', '.bup', '.ifo', '.vob', '.db', '.plist', '.ics', '.safariextz', '.vcf', '.pm6', '.scpt', '.dll', '.db', '.db', '.db'].indexOf(lower) !== -1)
            this.type = 'ignore';
        if (['.sfv', '.srr', '.url', '.md5'].indexOf(lower) !== -1)
            this.type = 'garbage';
        if (lower === '')
            this.type = 'unknown';
        if (!this.type) {
            if (this.debug)
                console.log('unknown type', lower, this.path);
        }
        if (this.type === 'unknown') {
            if (this.debug)
                console.log('unknown type', this.path);
        }
    };
    File.prototype.isValid = function () {
        var lower = this.filename.toLowerCase();
        var valid = true;
        if (lower.startsWith('.ds_store')) {
            valid = false;
        }
        if (lower.startsWith('._')) {
            valid = false;
        }
        return valid;
    };
    File.prototype.sha = function (divider) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        var processfilesize = Math.round(_this.size / divider);
                        var shasum = crypto.createHash('sha1');
                        var readable = fs.createReadStream(_this.path, {
                            start: 0,
                            end: processfilesize
                        });
                        readable.on('data', function (d) {
                            shasum.update(d);
                        });
                        readable.on('end', function () {
                            var d = shasum.digest('hex');
                            _this.sha1 = d;
                            return resolve(d);
                        });
                    })];
            });
        });
    };
    File.prototype.imageinfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var types;
            return __generator(this, function (_a) {
                types = ['bmp', 'cur', 'dds', 'gif', 'icns', 'ico', 'j2c', 'jp2', 'jpg', 'ktx', 'png', 'pnm', 'psd', 'svg', 'tiff', 'webp'];
                if (types.indexOf(this.ext.slice(1)) !== -1) {
                    this.image = imageSize.imageSize(this.path); // imageSize.types; // (this.path);
                    return [2 /*return*/, this.image];
                }
                else {
                    console.log("unknown extension " + this.path);
                    return [2 /*return*/, undefined];
                }
                return [2 /*return*/];
            });
        });
    };
    File.prototype.movieinfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ffprobe, height, width;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, utils_1.getFfprobe(this.path)];
                    case 1:
                        ffprobe = _a.sent();
                        height = 0;
                        width = 0;
                        ffprobe.streams.forEach(function (stream) {
                            if (stream.width && stream.width > width)
                                width = stream.width;
                            if (stream.height && stream.height > height)
                                height = stream.height;
                        });
                        this.video = Object.assign({}, ffprobe, {
                            width: width,
                            height: height,
                            duration: parseFloat(ffprobe.format.duration)
                        });
                        // this.video = {
                        // 	width: width,
                        // 	height: height,
                        // 	duration: parseFloat(ffprobe.format.duration)
                        // }
                        return [2 /*return*/, this.video];
                }
            });
        });
    };
    File.prototype.musicinfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, mm.parseFile(this.path, {
                        skipCovers: true,
                        duration: true,
                        skipPostHeaders: true,
                        includeChapters: false
                    })
                        .then(function (metadata) {
                        delete metadata.common.picture;
                        _this.audio = Object.assign({}, metadata.format, metadata.common);
                        // console.log(util.inspect(msg, { showHidden: false, depth: null }));
                        return _this.audio;
                    })
                        .catch(function (err) {
                        console.error(err.message);
                    })];
            });
        });
    };
    return File;
}());
exports.File = File;
var Files = /** @class */ (function () {
    function Files(debug) {
        if (debug === void 0) { debug = true; }
        this.debug = debug;
        this._files = [];
        this._sources = [];
    }
    Files.prototype.fromDirectory = function (dir, options) {
        return __awaiter(this, void 0, void 0, function () {
            var files;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        dir = utils_1.dirRemoveLastSlash(dir);
                        this._sources.push(dir);
                        return [4 /*yield*/, utils_1.getFilesFromDir(dir, options)];
                    case 1:
                        files = _a.sent();
                        files.forEach(function (filepath) {
                            var file = new File(filepath, _this.debug);
                            if (file.isValid()) {
                                _this._files.push(file);
                            }
                            else {
                                // console.log('invalid', file.path);
                            }
                        });
                        if (this.debug)
                            console.log('done', dir, options, files.length);
                        return [2 /*return*/, this];
                }
            });
        });
    };
    Files.prototype.fromDirectories = function (dirs, options) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, Promise.all(dirs.map(function (dir) { return _this.fromDirectory(dir, options); })).then(function () { return _this; })];
            });
        });
    };
    Object.defineProperty(Files.prototype, "all", {
        get: function () {
            return this._files;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Files.prototype, "length", {
        get: function () {
            return this._files.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Files.prototype, "filetypes", {
        get: function () {
            return this._files.reduce(function (prev, current, index) {
                if (prev.indexOf(current.type) === -1) {
                    prev.push(current.type);
                }
                return prev;
            }, []);
        },
        enumerable: true,
        configurable: true
    });
    Files.prototype.byType = function (type) {
        return this._files.filter(function (file) { return file.type === type; });
    };
    Files.prototype.byTypes = function (types) {
        return this._files.filter(function (file) { return types.indexOf(file.type) !== -1; });
    };
    // byRegex(field: string, regex: RegExp): Array<File> {
    // 	return this._files.filter(file => regex.test(file[field].toString()))
    // }
    Files.prototype.withoutType = function (type) {
        return this._files.filter(function (file) { return file.type !== type; });
    };
    Files.prototype.withoutTypes = function (types) {
        return this._files.filter(function (file) { return types.indexOf(file.type) === -1; });
    };
    Files.prototype.findRelated = function (inputfile) {
        if (['movie', 'nfo'].indexOf(inputfile.type) === -1) {
            throw new Error('dont know how to find related of ' + inputfile.type);
        }
        var related = [];
        if (inputfile.type === 'movie') {
            related = this._files.filter(function (file) {
                var letthrough = false;
                if (file.path !== inputfile.path && file.dir === inputfile.dir && file.type !== 'movie') {
                    if (file.base === inputfile.base || file.base === inputfile.base + '-thumbs') {
                        letthrough = true;
                    }
                }
                return letthrough;
            });
        }
        if (inputfile.type === 'nfo') {
            related = this._files.filter(function (file) {
                var letthrough = false;
                if (file.path !== inputfile.path && file.dir === inputfile.dir && file.type !== 'nfo') {
                    if (file.base === inputfile.base || file.base === inputfile.base + '-thumbs') {
                        letthrough = true;
                    }
                }
                return letthrough;
            });
        }
        if (this.debug) {
            if (related.length > 2) {
                related.forEach(function (file) {
                    console.log(inputfile.path, file.type, file.path);
                });
                console.log();
            }
        }
        return related;
    };
    return Files;
}());
exports.Files = Files;
//# sourceMappingURL=files.js.map